<?php

namespace Model;


use Ostendis\StoredProcedure\StoredProcedureRecord;

/**
 * Class SpQueryAllTestModel
 *
 * @package Model
 */
class SpQueryAllTestModel extends StoredProcedureRecord
{
    /** @var integer */
    public $id;

    /** @var string */
    public $firstName;

    /** @var string */
    public $lastName;

    /** @var string */
    public $email;

    /** @var integer */
    public $active;

    /** @var string */
    public $accountString;

    /** @var integer */
    public $order;


    /**
     * {@inheritdoc}
     */
    public static function spLoadNames(): array
    {
        return [
            'company' => 'ODM.getCompanyAccounts',
            'pro'     => 'ODM.getCompanyProAccountNamesCbo',
        ];
    }

    public static function getPropertyMap(): array
    {
        return [
            'AccountEmail'     => 'email',
            'AccountFirstName' => 'firstName',
            'AccountID'        => 'id',
            'AccountLastName'  => 'lastName',
            'AccountActive'    => 'active',
            'AccountString'    => 'accountString',
            'OrderColumn'      => 'order',
        ];
    }

    public function rules()
    {
        return [
            [['accountString', 'email', 'firstName', 'lastName',], 'trim'],
            [['id'], 'required'],

            [['active', 'id', 'order'], 'integer'],
            [['accountString', 'email', 'firstName', 'lastName'], 'string'],
        ];
    }


}
