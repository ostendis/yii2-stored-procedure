<?php

namespace Model;


use Ostendis\StoredProcedure\StoredProcedureRecord;

/**
 * Class SpRecordTestModel
 *
 * @package Model
 */
class SpRecordTestModel extends StoredProcedureRecord
{
    /** @var integer */
    public $id;

    /** @var string */
    public $title;

    public $isActive;


    public static function getPropertyMap(): array
    {
        return [
            'testID'     => 'id',
            'testTitle'  => 'title',
            'testStatus' => 'isActive',
        ];
    }

    public function rules()
    {
        return [
            [['title'], 'trim'],
            [['id', 'title', 'isActive'], 'required'],

            [['id'], 'integer'],
            [['title'], 'string'],
            [['isActive'], 'boolean'],
        ];
    }


}
