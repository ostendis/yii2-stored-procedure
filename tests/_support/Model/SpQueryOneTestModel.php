<?php

namespace Model;


use Ostendis\StoredProcedure\StoredProcedureRecord;

/**
 * Class SpQueryOneTestModel
 *
 * @package Model
 */
class SpQueryOneTestModel extends StoredProcedureRecord
{
    /** @var integer */
    public $id;

    /** @var string */
    public $title;

    /** @var \DateTime */
    public $createdAt;

    /**
     * {@inheritdoc}
     */
    public static function spLoadNames(): array
    {
        return [
            'header' => 'ODM.getJobHeader',
            'list'   => 'ODM.getJobs',
        ];
    }

    public static function getPropertyMap(): array
    {
        return [
            'JobID'          => 'id',
            'JobTitle'       => 'title',
            'JobCreatedTime' => 'createdAt',
        ];
    }

    public function rules()
    {
        return [
            [['title'], 'trim'],
            [['id', 'title', 'createdAt'], 'required'],

            [['id'], 'integer'],
            [['title'], 'string'],
        ];
    }


}
