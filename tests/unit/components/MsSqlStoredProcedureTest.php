<?php


use Codeception\Test\Unit;
use Model\SpRecordTestModel;

class MsSqlStoredProcedureTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    /** @var \Ostendis\StoredProcedure\MsSqlStoredProcedure */
    private $sp;

    public function testSingleResultSetSp()
    {
        $this->sp->setName('ODM.getJobs');
        $this->sp->setParams([
            'AccountID'       => 100015,
            'AccountLangCode' => 'DE',
            'ArchivedJobs'    => 0,
        ]);
        $this->sp->execute();

        expect($this->sp->hasResultSets())->true();
        expect($this->sp->hasOutputParams())->false();

        $results = $this->sp->getResultSets();
        expect(count($results))->equals(1);
        expect(count($results[0]))->equals(4);

        expect($results[0][0]['JobID'])->equals('10210');
        expect($results[0][0]['JobTitle'])->equals('Kundenberater/in / Key-Account-Manager/in');
        expect($results[0][0]['JobCreatedTime'])->equals('2015-07-05 15:56:14.917');

        expect($results[0][1]['JobID'])->equals('10211');
        expect($results[0][1]['JobTitle'])->equals('Webprogrammierer/in 80 - 100%');
        expect($results[0][1]['JobCreatedTime'])->equals('2015-07-05 15:56:14.957');
    }

    public function testMultiResultSetSp()
    {
        $this->sp->setName('ORS.getReport');
        $this->sp->setParams([
            'AccountID'       => 100001,
            'AccountLangCode' => 'DE',
            'ReportID'        => 20,
        ]);
        $this->sp->execute();

        expect($this->sp->hasResultSets())->true();
        expect($this->sp->hasOutputParams())->false();

        $results = $this->sp->getResultSets();
        expect(count($results))->equals(3);
        expect(count($results[0]))->equals(1);
        expect(count($results[1]))->equals(12);
        expect(count($results[2]))->equals(1);

        expect($results[0][0]['ReportChartTypeID'])->equals('20');
        expect($results[0][0]['ReportChartType'])->equals('ColumnChart');
        expect($results[0][0]['ReportThresholdSwitchToTableChart'])->equals('24');
    }

    public function testSingleOutputParamSp()
    {
        $this->sp->setName('ODM.chkDossierAttachmentPermission');
        $this->sp->setParams([
            'AccountID'                => 100001,
            'DossierViewID'            => 1,
            'DossierAttachmentID'      => 1687,
            'DossierAttachmentStorage' => 'OBM',
        ]);
        $this->sp->setInParams([
            'PermissionOK' => [
                'type'    => \yii\db\mssql\PDO::PARAM_BOOL,
                'default' => 0,
            ],
        ]);
        $this->sp->execute();

        expect($this->sp->hasResultSets())->false();
        expect($this->sp->hasOutputParams())->true();

        $output = $this->sp->getOutputParams();
        expect(count($output))->equals(1);

        expect($output['PermissionOK'])->equals('1');
    }

    public function testMultiOutputParamSp()
    {
        $this->sp->setName('ODM.newjobduplicate');
        $this->sp->setParams([
            'AccountID'        => 100001,
            'AccountLangCode'  => 'DE',
            'JobIDtoDuplicate' => 10385,
            'JobTitleNew'      => 'JobDuplikat',
        ]);
        $this->sp->setInParams([
            'MessageID'   => [
                'type'    => \yii\db\mssql\PDO::PARAM_INT,
                'default' => 0,
            ],
            'MessageText' => [
                'type'    => \yii\db\mssql\PDO::PARAM_STR,
                'default' => '',
                'length'  => 500,
            ],
        ]);
        $this->sp->execute();

        expect($this->sp->hasResultSets())->false();
        expect($this->sp->hasOutputParams())->true();

        $output = $this->sp->getOutputParams();
        expect(count($output))->equals(2);

        expect($output['MessageID'])->equals('0');
        expect($output['MessageText'])->equals('');
    }

    public function testMultiOutputAndResultsSp()
    {
        $this->sp->setName('ODM.getJobIDinfos');
        $this->sp->setParams([
            'AccountLangCode' => 'DE',
            'JobID'           => 10211,
        ]);
        $this->sp->setInParams([
            'MessageID'   => [
                'type'    => \yii\db\mssql\PDO::PARAM_INT,
                'default' => 0,
            ],
            'MessageText' => [
                'type'    => \yii\db\mssql\PDO::PARAM_STR,
                'default' => '',
                'length'  => 500,
            ],
        ]);
        $this->sp->execute();

        expect($this->sp->hasResultSets())->true();
        expect($this->sp->hasOutputParams())->true();

        $results = $this->sp->getResultSets();
        expect(count($results))->equals(1);
        expect(count($results[0]))->equals(1);

        expect($results[0][0]['CompanyName'])->equals('TestOstendisFirma');
        expect($results[0][0]['JobTitle'])->equals('Webprogrammierer/in 80 - 100%');

        $output = $this->sp->getOutputParams();
        expect(count($output))->equals(2);

        expect($output['MessageID'])->equals('0');
        expect($output['MessageText'])->equals('');
    }

    public function testUtf8()
    {
        $this->sp->setName('ODM.newDossierViewAnswer2');
        $this->sp->setParams([
            'AccountID'                             => 101452,
            'AccountLangCode'                       => 'DE',
            'DossierViewID'                         => 7584,
            'DossierViewAnswerSentEmailOfAccountID' => 101452,
            'DossierViewAnswerTypeID'               => 50,
            'DossierViewAnswerMediumID'             => 1,
            'DossierViewAnswerTextSubject'          => '✅ Zusage für ein lässiges Vorstellungsgespräch....',
            'DossierViewAnswerText'                 => 'Telefonisches Vorstellungsgespräch! 😉',
            'DossierViewAnswerCcEmailAddresses'     => null,
            'DossierViewAnswerProcessedBy'          => null,
        ]);
        $this->sp->setInParams([
            'MessageID'   => [
                'type'    => \yii\db\mssql\PDO::PARAM_INT,
                'default' => 0,
            ],
            'MessageText' => [
                'type'    => \yii\db\mssql\PDO::PARAM_STR,
                'default' => '',
                'length'  => 500,
            ],
        ]);
        $this->sp->execute();

        expect($this->sp->hasOutputParams())->true();

        $output = $this->sp->getOutputParams();
        expect(count($output))->equals(2);

        expect($output['MessageID'])->equals(70000);
    }

    protected function _before()
    {
        $this->sp = new Ostendis\StoredProcedure\MsSqlStoredProcedure(['model' => SpRecordTestModel::class]);
    }

    protected function _after()
    {
    }
}
