<?php



class StoredProcedureQueryTest extends \Codeception\Test\Unit
{
    /** @var \UnitTester */
    protected $tester;

    private $modelClassOne = '\Model\SpQueryOneTestModel';
    private $modelClassAll = '\Model\SpQueryAllTestModel';

    public function testQueryOne()
    {
        $spq = new \Ostendis\StoredProcedure\StoredProcedureQuery($this->modelClassOne);
        $spq->params([
            'header' => [
                'AccountID'       => 100015,
                'AccountLangCode' => 'DE',
                'JobID'           => 10210,
            ],
        ]);
        $result = $spq->one(['header']);

        expect(is_null($result))->equals(false);
        expect(is_object($result))->equals(true);
        expect($result)->isInstanceOf($this->modelClassOne);
    }

    public function testQueryAll()
    {
        $spq = new \Ostendis\StoredProcedure\StoredProcedureQuery($this->modelClassAll);
        $spq->params([
            'company' => [
                'AccountID' => 100001,
            ],
            'pro'     => [
                'AccountID'                                => 100001,
                'AccountLangCode'                          => 'DE',
                'withDeletedAccountsStillUsesInActiveJobs' => true,
            ],
        ]);
        $result = $spq->all(['company', 'pro']/*,'AccountID'*/);

        expect(is_array($result))->equals(true);
        expect(count($result))->equals(4);
        expect($result[0])->isInstanceOf($this->modelClassAll);
    }

    protected function _before()
    {
    }

    // tests

    protected function _after()
    {
    }
}
