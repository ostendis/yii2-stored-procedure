<?php

use Model\SpRecordTestModel;

class StoredProcedureRecordTest extends \Codeception\Test\Unit
{
    /** @var \UnitTester */
    protected $tester;


    public function testLoadNames()
    {
        expect(SpRecordTestModel::spLoadNames())->equals(['getsprecordtestmodel']);
    }

    public function testSaveNames()
    {
        expect(SpRecordTestModel::spSaveNames())->equals(['setsprecordtestmodel']);
    }

    public function testLoad()
    {
        $model = new SpRecordTestModel();
        $model->spLoad([
            'testID'        => 1,
            'testTitle'     => 'This is a test',
            'testStatus'    => true,
            'somethingElse' => ['asdf'],
        ]);

        expect($model->id)->equals(1);
        expect($model->title)->equals('This is a test');
        expect($model->isActive)->true();
    }

    /*
    // FIXME: Implement method in component
    public function testSave()
    {
        $model = new SpRecordTestModel();
        $model->spLoad([
            'id'       => 1,
            'title'    => 'This is a test',
            'isActive' => true,
        ]);

        expect($model->spSave())->true();
    }
    */

    public function testAttributeTypes()
    {
        $model = new SpRecordTestModel();
        $types = $model->getAttributeTypes();

        expect($types['id'])->equals('integer');
        expect($types['title'])->equals('string');
        expect($types['isActive'])->equals(null); // `null` because of no type hint
    }


    protected function _before()
    {
    }

    protected function _after()
    {
    }
}
