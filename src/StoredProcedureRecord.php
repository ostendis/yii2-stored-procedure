<?php

namespace Ostendis\StoredProcedure;

use Ostendis\StoredProcedure\models\Model;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\Connection;
use yii\db\Exception;
use yii\helpers\StringHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * StoredProcedureRecord is an equivalent to ActiveRecord
 * It uses Stored Procedures instead of bare tables to read/write data
 *
 * @package   Ostendis\StoredProcedure
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class StoredProcedureRecord extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    const EVENT_BEFORE_LOAD = 'beforeSpRecordLoad';
    const EVENT_AFTER_LOAD = 'afterSpRecordLoad';

    /** @var Connection */
    public static $db = null;

    /**
     * Get name of reading stored procedure for this model
     *
     * @return array
     */
    public static function spLoadNames(): array
    {
        return ['get' . strtolower(StringHelper::basename(get_called_class()))];
    }

    /**
     * Get name of writing stored procedure for this model
     *
     * @return array
     */
    public static function spSaveNames(): array
    {
        return ['set' . strtolower(StringHelper::basename(get_called_class()))];
    }

    /**
     * {@inheritdoc}
     * @return Connection|object
     * @throws InvalidConfigException
     */
    public static function getDb()
    {
        if (self::$db !== null && self::$db instanceof Connection) {
            return self::$db;

        } else if (Yii::$app->has('dbSp') && Yii::$app->get('dbSp') instanceof Connection) {
            self::$db = Yii::$app->get('dbSp');
            return self::$db;
        }

        return Yii::$app->getDb();
    }

    /**
     * Explicitly set a different DB Connection for a model class
     *
     * @param $connection
     * @throws InvalidConfigException
     */
    public static function setDb($connection): void
    {
        if ($connection instanceof Connection) {
            self::$db = $connection;

        } else if (is_string($connection) && Yii::$app->has($connection) && Yii::$app->get($connection) instanceof Connection) {
            self::$db = Yii::$app->get($connection);

        } else {
            throw new InvalidArgumentException('Given argument `$connection` must be of type `string` or `\yii\db\Connection`');
        }
    }

    /**
     * Get property map of stored procedure and model
     *
     * Format must be:
     * ```
     *   [
     *     'spProperty1' => 'modelProperty',
     *     'spProperty2' => 'someProperty',
     *     'spProperty3' => 'rel.otherProperty', // Relation-properties need a prefix and dot `.`
     *   ];
     * ```
     * Note that multiple SP properties can point to the same model property.
     * This is important if a model calls multiple SP's which deliver overlapping results.
     *
     * @return array
     */
    public static function getPropertyMap(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * @return StoredProcedureQuery the newly created [[StoredProcedureQuery]] instance.
     * @throws InvalidConfigException
     */
    public static function find()
    {
        return Yii::createObject(StoredProcedureQuery::class, [get_called_class()]);
    }

    /**
     * Load SP data into model and type-cast attribute values
     *
     * SP data gets loaded into model and values type-casted into the correct type before assigning
     * This is necessary because MSSQL delivers:
     * - booleans as int
     * - int as string
     * - float as string
     *
     * @param array $data
     * @param bool  $useValidation
     * @return bool
     */
    public function spLoad(array $data, bool $useValidation = true): bool
    {
        $this->trigger(self::EVENT_BEFORE_LOAD);
        if (count($data)) {
            $map = static::getPropertyMap();
            $mappedData = [];

            foreach ($data as $key => $value) {
                if (isset($map[$key])) {
                    $attributeName = $map[$key];
                    $mappedData[$attributeName] = $value;
                }
            }

            $this->setAttributes($mappedData, false);
            $this->resolveRelations($mappedData);

            $this->trigger(self::EVENT_AFTER_LOAD);
            $this->afterFind();
            return $useValidation ? $this->validate() : true;
        }
        $this->trigger(self::EVENT_AFTER_LOAD);
        return false;
    }

    /**
     * @param StoredProcedureRecord|string                           $modelClass
     * @param array                                                  $params
     * @param                                                        $pk
     * @return array
     * @throws InvalidConfigException
     * @throws Exception
     * @throws BadRequestHttpException
     */
    public function hasMany($modelClass, $params, $pk): array
    {
        $spKeys = array_keys($params);
        $models = $modelClass::find()->parent($this)->params($params)->all($spKeys, $pk);

        return $models;
    }

    /**
     * @param StoredProcedureRecord|string                           $modelClass
     * @param array                                                  $params
     * @param                                                        $pk
     * @return array|null
     * @throws InvalidConfigException
     * @throws Exception
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function hasOne($modelClass, $params, $pk): ?array
    {
        $spKeys = array_keys($params);
        $models = $modelClass::find()->parent($this)->params($params)->one($spKeys, $pk);

        return $models;
    }

    /**
     * Save model to DB via stored procedure
     *
     * @return bool
     */
    public function spSave(): bool
    {
        // TODO: Must be implemented
        // Needs $map = ArrayHelper::invertKeyValue(static::getPropertyMap());
        return false;
    }

}
