<?php

namespace Ostendis\StoredProcedure;

use Ostendis\StoredProcedure\models\Model;
use yii\base\Component;
use yii\web\BadRequestHttpException;

/**
 * Class StoredProcedureQuery
 *
 * @package   Ostendis\StoredProcedure
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class StoredProcedureQuery extends Component
{
    const EVENT_BEFORE_SP_ALL = 'beforeSpQueryExecAll';
    const EVENT_AFTER_SP_ALL = 'afterSpQueryExecAll';
    const EVENT_BEFORE_SP_ONE = 'beforeSpQueryExecOne';
    const EVENT_AFTER_SP_ONE = 'afterSpQueryExecOne';

    /** @var string The name of the StoredProcedureRecord class */
    public $modelClass;

    /** @var string The primary key to filter for form multiple results */
    public $primaryKey = null;

    /** @var integer|null The parent model id */
    public $parentId = null;

    /** @var \Ostendis\StoredProcedure\MsSqlStoredProcedure[] */
    protected $storedProcedures;

    // TODO: Add `result` property and `first()` method

    /**
     * Constructor.
     *
     * @param string $modelClass the model class associated with this query
     * @param array  $config     configurations to be applied to the newly created query object
     */
    public function __construct($modelClass, $config = [])
    {
        $this->modelClass = $modelClass;
        foreach (($this->modelClass)::spLoadNames() as $spKey => $spName) {
            $this->storedProcedures[$spKey] = new MsSqlStoredProcedure([
                'model' => $this->modelClass,
                'name'  => $spName,
            ]);
        }
        parent::__construct($config);
    }

    /**
     * Set params for current query
     *
     * @param array $params
     * @return \Ostendis\StoredProcedure\StoredProcedureQuery
     */
    public function params(array $params): self
    {
        foreach ($this->storedProcedures as $spKey => $sp) {
            if (isset($params[$spKey])) {
                $sp->setParams($params[$spKey]);
            }
        }
        return $this;
    }

    /**
     * Set specific primary key to filter from multiple results
     *
     * @param string $pk
     * @return $this
     */
    public function byPk(string $pk)
    {
        $this->primaryKey = $pk;

        return $this;
    }

    /**
     * Set parent model
     *
     * @param \Ostendis\StoredProcedure\models\Model | integer $parent
     * @return $this
     */
    public function parent($parent)
    {
        if ($parent instanceof Model) {
            $this->parentId = $parent->id;
        } else if (is_integer($parent)) {
            $this->parentId = $parent;
        }

        return $this;
    }

    /**
     * Set in-params for current query
     *
     * @param array $inParams
     * @return \Ostendis\StoredProcedure\StoredProcedureQuery
     */
    public function inParams(array $inParams): self
    {
        foreach ($this->storedProcedures as $spKey => $sp) {
            if (isset($inParams[$spKey])) {
                $sp->setInParams($inParams[$spKey]);
            }
        }
        return $this;
    }

    /**
     * Query all results, load and return them as array
     *
     * @param string|array|null $spList SPs to execute
     * @param string|array      $pk
     * @return \Ostendis\StoredProcedure\StoredProcedureRecord[]|array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function all($spList = null, $pk = null): array
    {
        if (is_string($spList)) $spList = [$spList];
        $this->trigger(self::EVENT_BEFORE_SP_ALL);

        $result = [];
        $indexedResult = [];
        foreach ($this->storedProcedures as $spKey => $sp) {
            if ($spList !== null && !in_array($spKey, $spList)) continue;
            $this->trigger(self::EVENT_BEFORE_SP_ONE, new StoredProcedureEvent(['storedProcedure' => $sp]));
            $sp->execute();
            if (!$sp->hasResultSets()) continue;

            $dataSets = $sp->getResultSets();

            foreach ($dataSets as $set) {
                foreach ($set as $col => $row) {
                    $index = $this->generateListIndex($row, $pk);
                    if ($this->primaryKey !== null && $this->primaryKey !== $index) continue;

                    /** @var \Ostendis\StoredProcedure\StoredProcedureRecord $model */
                    $model = isset($indexedResult[$index]) ? $indexedResult[$index] : new $this->modelClass();

                    if (!$model->spLoad($row, false)) {
                        $errors = $model->getFirstErrors();
                        throw new BadRequestHttpException(array_shift($errors));
                    }

                    $model->setParentId($this->parentId);
                    $indexedResult[$index] = $model;
                }
            }
            $this->trigger(self::EVENT_AFTER_SP_ONE, new StoredProcedureEvent(['storedProcedure' => $sp]));

        }

        $this->trigger(self::EVENT_AFTER_SP_ALL);
        foreach ($indexedResult as $resultModel) {
            /** @var \Ostendis\StoredProcedure\StoredProcedureRecord $resultModel */
            $resultModel->afterFindComplete();
            $result[] = $resultModel;
        }
        return $result;
    }

    /**
     * Query one result, load and return it
     *
     * The optional key param lets you load the model for all or just a single SP
     *
     * @param string|array|null $spList SPs to execute
     * @param string            $pk
     * @return \Ostendis\StoredProcedure\StoredProcedureRecord|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function one($spList = null, string $pk = null)
    {
        $hasResult = false;
        /** @var \Ostendis\StoredProcedure\StoredProcedureRecord $model */
        $model = new $this->modelClass();

        if (is_string($spList)) $spList = [$spList];

        $this->trigger(self::EVENT_BEFORE_SP_ALL);
        foreach ($this->storedProcedures as $spKey => $sp) {
            if ($spList !== null && !in_array($spKey, $spList)) continue;
            $this->trigger(self::EVENT_BEFORE_SP_ONE, new StoredProcedureEvent(['storedProcedure' => $sp]));

            $sp->execute();
            if (!$sp->hasResultSets()) continue;

            $hasResult = true;
            $dataSets = $sp->getResultSets();
            $data = [];

            if (count($dataSets[0]) > 1) {
                // Find the right entry within multiple results
                foreach ($dataSets[0] as $item) {
                    if ($pk !== null && isset($item[$pk]) && $this->primaryKey !== null && (string)$item[$pk] === $this->primaryKey) {
                        $data = $item;
                    }
                }
            } else {
                $data = $dataSets[0][0];
            }

            if (!$model->spLoad($data, false)) {
                throw new BadRequestHttpException("{$this->modelClass} could not be populated with data");
            }

            $model->setParentId($this->parentId);

            $this->trigger(self::EVENT_AFTER_SP_ONE, new StoredProcedureEvent(['storedProcedure' => $sp]));
        }
        $this->trigger(self::EVENT_AFTER_SP_ALL);
        $model->afterFindComplete();

        if (!$hasResult) {
            return null;
        }

        return $model;
    }

    /**
     * Generate a key for the items of a model array
     * The key is needed to select specific items on the client
     *
     * @param array        $row The data row
     * @param array|string $key One or more column names
     * @return string
     */
    protected function generateListIndex(array $row, $key): string
    {
        if ($key === null) {
            $columnName = array_keys($row)[0];
            $index = $row[$columnName];

        } else if (is_array($key)) {
            $indexParts = [];
            foreach ($key as $value) {
                $indexParts[] = $row[$value];
            }
            $index = implode('.', $indexParts);

        } else {
            $index = $row[$key];
        }

        return $index;
    }

}
