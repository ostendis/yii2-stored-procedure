<?php

namespace Ostendis\StoredProcedure;


use yii\base\Event;

/**
 * Class StoredProcedureEvent
 *
 * @package   Ostendis\StoredProcedure
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class StoredProcedureEvent extends Event
{
    /** @var \Ostendis\StoredProcedure\MsSqlStoredProcedure */
    public $storedProcedure;

}
