<?php

namespace Ostendis\StoredProcedure\models;

use Ostendis\Utilities\helpers\TypeHelper;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Yii;

/**
 * (Stored Procedure) Model is an equivalent to BaseActiveRecord
 * It
 *
 * @package   Ostendis\StoredProcedure\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class Model extends \yii\base\Model
{
    /** @event Event An event that is triggered after the model has been populated with data */
    const EVENT_AFTER_POPULATE = 'afterPopulate';

    /** @event Event an event that is triggered after the record is created and populated with an SP result. */
    const EVENT_AFTER_FIND = 'afterFind';

    /** @event Event an event that is triggered after the record is created and populated with all SP results. */
    const EVENT_AFTER_FIND_COMPLETE = 'afterFindComplete';

    /** @var integer */
    public $id;

    /** @var integer */
    private $_parentId;


    /**
     * Get map of relations between models and their properties
     *
     * Format must be:
     * ```
     *   [
     *     'modelRelationProperty' => [
     *       SomeModel::class,
     *       [
     *         'modelProperty' => 'dbProperty',
     *         'relationProperty' => 'rel.something', // Relation-property with prefix and dot
     *         // ...
     *       ],
     *     ],
     *     'secondModelRelationProperty' => [
     *       OtherModel::class,
     *       [
     *         // ...
     *       ],
     *     ],
     *   ];
     * ```
     *
     * @return array
     */
    public static function getRelationMap(): array
    {
        return [];
    }

    /**
     * Get types of model attributes based on DocComment annotation
     *
     * @return array
     * @throws ReflectionException
     */
    public function getAttributeTypes(): array
    {
        $types = [];
        foreach ($this->attributes(false) as $attribute) {
            $types[$attribute] = $this->getAttributeType($attribute);
        }

        return $types;
    }

    /**
     * Overrides default implementation by adding a flag to get all attributes
     *
     * @param bool $publicOnly
     * @return array
     * @throws ReflectionException
     */
    public function attributes(bool $publicOnly = true)
    {
        return $publicOnly ? parent::attributes() : $this->allAttributes();
    }

    /**
     * Returns the list of all attribute names.
     * By default, this method returns all public, protected and private non-static properties of the class.
     *
     * @return array list of attribute names.
     * @throws ReflectionException
     */
    public function allAttributes()
    {
        $class = new ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    /**
     * Get type of model attribute based on DocComment annotation
     *
     * @param string $name
     * @return null|string
     */
    public function getAttributeType(string $name): ?string
    {
        try {
            $class = new ReflectionClass(get_called_class());
            $property = $class->getProperty($name);

            $doc = $property->getDocComment();
            if (!$doc) return null;

            $typePosStart = strpos($doc, '@var ') + 5;
            if (!$typePosStart) return null;

            $typePosEnd = strpos($doc, ' ', $typePosStart);
            if (!$typePosEnd) return null;

            return trim(substr($doc, $typePosStart, $typePosEnd - $typePosStart));

        } catch (ReflectionException $e) {
            Yii::error($e->getMessage(), __METHOD__);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $types = $this->getAttributeTypes();
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->allAttributes());

            foreach ($values as $name => $value) {
                if ($this->canSetProperty($name) && isset($attributes[$name])) {
                    $this->$name = TypeHelper::cast($types[$name], $value);
                } else if ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }

    /**
     * Resolve relation and create new models with the given data based on defined relation info
     *
     * @param array $data
     */
    public function resolveRelations(array $data): void
    {
        $relations = static::getRelationMap();
        if (!count($relations)) return;

        foreach ($relations as $relPropName => $relPropInfos) {
            if (!property_exists(static::class, $relPropName)) continue;
            if (!$this->isRelationInfoValid($relPropInfos)) continue;

            $relationClass = $relPropInfos[0];
            /** @var Model $relationModel */
            $relationModel = $this->$relPropName instanceof $relationClass ? $this->$relPropName : new $relationClass();
            $relationData = [];

            foreach ($relPropInfos[1] as $modelProp => $dbProp) {
                if (array_key_exists($dbProp, $data)) {
                    if (strpos($modelProp, '.') !== false) {
                        $relationData[$modelProp] = $data[$dbProp];
                    } else {
                        $relationModel->$modelProp = TypeHelper::cast($relationModel->getAttributeType($modelProp), $data[$dbProp]);
                    }
                }
            }

            $relationModel->_parentId = $this->id;
            $relationModel->resolveRelations($relationData);
            $this->$relPropName = $relationModel;
        }
    }

    public function afterPopulate()
    {
        $this->trigger(self::EVENT_AFTER_POPULATE);
    }

    /**
     * This method is called when the AR object is created and populated with the query result.
     * The default implementation will trigger an [[EVENT_AFTER_FIND]] event.
     * When overriding this method, make sure you call the parent implementation to ensure the
     * event is triggered.
     */
    public function afterFind()
    {
        $this->trigger(self::EVENT_AFTER_FIND);
    }

    /**
     * This method is called when the AR object is created and populated with the query result.
     * The default implementation will trigger an [[EVENT_AFTER_FIND]] event.
     * When overriding this method, make sure you call the parent implementation to ensure the
     * event is triggered.
     *
     * @param bool $useValidation
     */
    public function afterFindComplete()
    {
        $this->trigger(self::EVENT_AFTER_FIND_COMPLETE);

        $properties = array_keys(static::getRelationMap());
        if (!count($properties)) return;

        foreach ($properties as $property) {
            if (!property_exists(static::class, $property)) continue;
            if (!is_object($this->$property)) continue;

            /** @var Model $relationModel */
            $relationModel = $this->$property;
            $relationModel->afterPopulate();
        }
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->_parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId($parentId): void
    {
        $this->_parentId = $parentId;
    }

    protected function isRelationInfoValid(array $relationInfo): bool
    {
        $isValid = true;

        if (!isset($relationInfo[0]) || empty($relationInfo[0]) || !class_exists($relationInfo[0])) {
            //throw new InvalidConfigException("Invalid Relation Info: Class `{$relationInfo[0]}` is not set or does not exist");
            $isValid = false;
        }
        if (!isset($relationInfo[1])) {
            //throw new InvalidConfigException("Invalid Relation Info: Property map is not set");
            $isValid = false;
        }
        if (!isset($relationInfo[1]) || !is_array($relationInfo[1]) || count($relationInfo[1]) === 0) {
            //throw new InvalidConfigException("Invalid Relation Info: Property map must be a non-empty array");
            $isValid = false;
        }

        foreach ($relationInfo[1] as $modelProp => $dbProp) {
            if (!property_exists($relationInfo[0], $modelProp) && strpos($modelProp, '.') === false) {
                //throw new InvalidConfigException("Invalid Relation Info: Class `{$relationInfo[0]}` has no property `{$modelProp}`");
                $isValid = false;
            }
        }

        return $isValid;
    }

}
