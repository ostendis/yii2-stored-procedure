<?php

namespace Ostendis\StoredProcedure;

use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\Command;
use yii\db\Exception;
use yii\db\mssql\PDO;

/**
 * Class MsSqlStoredProcedure
 *
 * @package   Ostendis\StoredProcedure
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class MsSqlStoredProcedure extends Component
{
    /** @var string The name of the StoredProcedureRecord class */
    protected $model;

    /** @var Command The db command to execute the stored procedure */
    protected $command;

    /** @var string The stored procedure's name */
    protected $name;

    /** @var string The stored procedure's param string */
    protected $paramStr = '';

    /** @var array The parameters passed to the stored procedure */
    protected $params = [];

    /** @var array The input parameters which will result in output parameters */
    protected $inParams = [];

    /** @var array The result sets returned from the stored procedure */
    protected $resultSets = [];

    /** @var array The resulting output parameters returned from the stored procedure */
    protected $outputParams = [];

    /** @var bool Whether this SP has already been executed or not */
    protected $isExecuted = false;


    /**
     * Execute Stored Procedure
     *
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function execute()
    {
        if (!isset($this->name)) {
            throw new InvalidConfigException('Property `name` must be set');
        }

        $this->buildParamString();

        $this->command = ($this->model)::getDb()->createCommand(sprintf('EXEC %s %s', $this->name, $this->paramStr));
        $this->bindParams();
        $this->bindInOutParams();
        $this->command->execute();
        $this->isExecuted = true;

        if ($this->command->pdoStatement->columnCount() > 0) {
            do {
                $this->resultSets[] = $this->command->pdoStatement->fetchAll(PDO::FETCH_ASSOC);
            } while ($this->command->pdoStatement->nextRowset());
        }
        $this->resultSets = array_filter($this->resultSets);
    }

    /**
     * Get execution state
     *
     * @return bool
     */
    public function getIsExecuted(): bool
    {
        return $this->isExecuted;
    }

    /**
     * Set the stored procedure's name
     *
     * @return string The name of the stored procedure
     * @throws InvalidConfigException If `name` is not set
     */
    public function getName(): string
    {
        if (!isset($this->name)) {
            throw new InvalidConfigException('Property `name` must be set');
        }

        return $this->name;
    }

    /**
     * Set the stored procedure's name
     *
     * @param string $name  The name of the stored procedure
     * @throws InvalidArgumentException If an empty string is given
     */
    public function setName(string $name): void
    {
        if (strlen($name) < 1) {
            throw new InvalidArgumentException('Invalid argument given');
        }

        $this->name = $name;
    }

    /**
     * Get the stored procedure's parameters
     *
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Set the stored procedure's parameters
     *
     * @param array $params
     */
    public function setParams(array $params = []): void
    {
        $this->params = $params;
    }

    /**
     * Add a parameter the stored procedure's parameter list
     *
     * @param string $key
     * @param mixed  $value
     */
    public function addParam(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * Get the stored procedure's input parameters
     *
     * @return array
     */
    public function getInParams(): array
    {
        return $this->inParams;
    }

    /**
     * Set the stored procedure's input parameters
     *
     * @param array $inParams
     */
    public function setInParams(array $inParams = []): void
    {
        $this->inParams = $inParams;
    }

    /**
     * @return array An array of result sets
     */
    public function getResultSets(): array
    {
        return $this->resultSets;
    }

    /**
     * @return bool Whether it has result sets or not
     */
    public function hasResultSets(): bool
    {
        return count($this->resultSets) > 0;
    }

    /**
     * @return array An array of output parameters
     */
    public function getOutputParams(): array
    {
        return $this->outputParams;
    }

    /**
     * @return bool Whether it has output parameters or not
     */
    public function hasOutputParams(): bool
    {
        return count($this->outputParams) > 0;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    /**
     * Build the param string from array
     */
    protected function buildParamString(): void
    {
        $paramList = [];

        if (count($this->params)) {
            foreach ($this->params as $key => $value) {
                $paramList[] = sprintf('@%s=:%s', $key, $key);
            }
        }

        if (count($this->inParams)) {
            foreach ($this->inParams as $key => $value) {
                $paramList[] = sprintf('@%s=:%s', $key, $key);
            }
        }

        $this->paramStr .= implode(',', $paramList);
    }

    /**
     * Bind parameters to command
     */
    protected function bindParams(): void
    {
        if (count($this->params)) {
            // Pay attention to the `&$value`
            foreach ($this->params as $key => &$value) {
                $this->command->bindParam(':' . $key, $value);
            }
        }
    }

    /**
     * Bind output parameters to command
     *
     * Each output param gets initialized with the given default value and bound afterwards
     * Format of each param must be like one of the following:
     *
     * ```
     * 'MessageID' => [
     *      'type' => PDO::PARAM_INT,
     *      'default' => 0,
     * ],
     *
     * 'MessageText' => [
     *      'type' => PDO::PARAM_STR,
     *      'default' => '',
     *      'length' => 500,
     * ]
     * ```
     *
     * Note that in case of type=string (varchar) the length must be passed as well.
     * If an empty string is passed as default, the result gets truncated to only one character.
     *
     * Also the types must be set using the constants in `yii\db\mssql\PDO`
     *
     * @see https://docs.microsoft.com/en-us/sql/connect/php/pdostatement-bindparam?view=sql-server-2017
     * @throws InvalidConfigException
     */
    protected function bindInOutParams(): void
    {
        if (count($this->inParams)) {
            foreach ($this->inParams as $key => $value) {
                $this->outputParams[$key] = $value['default'];

                if ($value['type'] === PDO::PARAM_STR && !isset($value['length'])) {
                    throw new InvalidConfigException('Output-Parameters of type `string` must provide a length.');
                }

                $dataLength = $value['type'] === PDO::PARAM_STR ? $value['length'] : PDO::SQLSRV_PARAM_OUT_DEFAULT_SIZE;
                $this->command->bindParam(':' . $key, $this->outputParams[$key], $value['type'] | PDO::PARAM_INPUT_OUTPUT, $dataLength);
            }
        }
    }
}
