<?php
$params = require __DIR__ . '/test_params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id'         => 'yii2-stored-procedure-tests',
    'basePath'   => dirname(__DIR__),
    'language'   => 'de',
    'components' => [
        'dbSp' => $db,
    ],

    'modules' => [
    ],

    'params' => $params,
];
